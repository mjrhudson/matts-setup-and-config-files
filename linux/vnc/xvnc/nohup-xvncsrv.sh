#!/bin/bash

nohup vncserver -geometry 1440x900 -rfbauth "$HOME/.vnc/passwd" -rfbport 59001 1>"$HOME/.local/scripts/vnc/xvnc/xvnc.log" 2>&1 &
echo $! > "$HOME/.local/scripts/vnc/xvnc/xvnc-pid.txt"
