#!/bin/bash

nohup x11vnc -xkb -forever -loop -repeat -noxrecord -noxfixes -noxdamage -display :0 -scale 0.90x0.90 -auth guess -rfbauth "$HOME/.vnc/passwd" -rfbport 59001 -shared 1>"$HOME/.local/scripts/vnc/x11vnc/x11vnc.log" 2>&1 &
echo $! > "$HOME/.local/scripts/vnc/x11vnc/x11vnc-pid.txt"
sleep 1
xrandr -d :0 --output DVI-D-0 --off
