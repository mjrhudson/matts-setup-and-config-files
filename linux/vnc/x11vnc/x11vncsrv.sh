#!/bin/bash

x11vnc -xkb -forever -loop -repeat -noxrecord -noxfixes -noxdamage -display :0 -scale 0.90x0.90 -auth guess -rfbauth "$HOME/.vnc/passwd" -rfbport 59001 -shared
sleep 1
xrandr -d :0 --output DVI-D-0 --off
