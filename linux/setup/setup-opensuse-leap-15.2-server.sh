#!/bin/bash

#             Matt's Setup and Configuration Files
#               Copyright (C) 2021  Matt Hudson
#                   Email: matt@mjrhudson.uk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

###########################################################################
#           TITLE: setup-opensuse-leap-15.2-server.sh                     #
#         AUTHORS: Matt Hudson (matt@mjrhudson.uk)                        #
#     DESCRIPTION: Bash script for configuring a fresh install of         #
#                  OpenSUSE Leap 15.2 (Server) to my liking               #
# INITIAL RELEASE: 14-June-2021                                           #
#   LAST MODIFIED: 14-June-2021                                           #
###########################################################################

# Note: I don't use the -y flag for zypper installs as I don't leave this
#       unattended, and just like to double check what is being installed as it
#       happens. Feel free to change this for an automated install.

# ******************************* START HERE ******************************
# Set configured=true when you have looked over the run options and
# configuration settings for this script and are happy with them. The script
# will not run until this is done. I've included this as a reminder to myself
# to configure my scripts.
configured=false
# *************************************************************************

# Configure run steps
# Set these to either true or false depending on which steps you want to run
run_step[0]=$configured # See above
run_step[1]=true        # Create/remove directories
run_step[2]=true        # Remove bloat
run_step[3]=true        # Install ClamAV
run_step[4]=true        # Configure firewalld
run_step[5]=true        # Install git
run_step[6]=true        # Install general software and utilities
run_step[7]=true        # Install TeX Live
run_step[8]=true        # Install development patterns
run_step[9]=true        # Install MBAT
run_step[10]=true       # Create SSH keys
run_step[11]=true       # Configure git
run_step[12]=true       # Configure SSH
run_step[13]=true       # Configure SSHD
run_step[14]=true       # Configure .bashrc

if [ "$configured" = false ]; then
    printf "\nConfigure this script first before running it!\n\n"
    exit
fi

# Configure settings
home_zone_name="example" # Name to give to home firewall zone
git_user_name="First Last" # Real name for git commits
git_user_email="example@example.com" # Contact email for git commits
ssh_port="22" # SSH port to use (22 is default, change it to something else)
vnc_ports="5900-5905" # VNC ports to use (590X is default, change it to something else)

# Set $reboot_required to false
reboot_required=false

# Make temporary directory for downloading/extracting
tempdir="/tmp/setup-opensuse-leap-15.2-server"
if [ -d "$tempdir" ]; then
    rm -rf "$tempdir"
fi
mkdir "$tempdir"
pushd "$tempdir"

# Create/remove directories
step=1
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Create/remove directories *****\n\n"
    if [ -d "$HOME/bin" ]; then
        rm -rf "$HOME/bin"
    fi
    if [ ! -d "$HOME/.local/bin" ]; then
        mkdir -p "$HOME/.local/bin"
    fi
    if [ ! -d "$HOME/.local/scripts" ]; then
        mkdir -p "$HOME/.local/scripts"
    fi
    if [ ! -d "$HOME/.config/systemd/user" ]; then
        mkdir -p "$HOME/.config/systemd/user"
    fi
fi

# Remove bloat
step=2
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Remove bloat *****\n\n"
    sudo zypper remove -y mutt mutt-doc mutt-lang
fi

# Install ClamAV
step=3
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install ClamAV *****\n\n"
    sudo zypper install clamav
    sudo freshclam
fi

# Configure firewalld
step=4
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Configure firewalld *****\n\n"
    sudo firewall-cmd --permanent --new-zone="$home_zone_name"
    sudo firewall-cmd --reload
    sudo firewall-cmd --set-default-zone "$home_zone_name"
fi

# Install git
step=5
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install git *****\n\n"
    sudo zypper install git
fi

# Install general software and utilities
step=6
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install general software and utilities *****\n\n"
    sudo zypper install htop zip unzip exfat-utils fuse-exfat
    # sudo zypper install neofetch octave
fi

# Install TeX Live
step=7
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install TeX Live *****\n\n"
    sudo zypper install texlive-scheme-full
fi

# Install development patterns
step=8
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install development patterns *****\n\n"
    sudo zypper install -t pattern devel_basis devel_mono devel_C_C++
fi

# Install MBAT
step=9
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install MBAT *****\n\n"
    git clone "https://gitlab.com/mjrhudson/mbat.git" "./mbat"
    pushd "./mbat"
    chmod +x "install-unix.sh"
    bash "./install-unix.sh"
    popd
    rm -rf "./mbat"
fi

# Create SSH keys
step=10
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Create SSH keys *****\n\n"
    ssh-keygen -t ed25519 -a 100 -f "$HOME/.ssh/id_ed25519" -C "$USER@$HOSTNAME"
    ssh-keygen -t ed25519 -a 100 -f "$HOME/.ssh/git_ed25519" -C "$USER@$HOSTNAME"
fi

# Configure git
step=11
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Configure git *****\n\n"
    git config --global user.name "$git_user_name"
    git config --global user.email "$git_user_email"
    git config --global core.editor "nano"
fi

# Configure SSH
step=12
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Configure SSH *****\n\n"
    if [ -f "$HOME/.ssh/config" ]; then
        rm -f "$HOME/.ssh/config"
    fi
    wget -P "$HOME/.ssh" "https://gitlab.com/mjrhudson/matts-setup-and-config-files/-/raw/master/linux/ssh/config"
    chmod 600 "$HOME/.ssh/config"
fi

# Configure SSHD
step=13
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Configure SSHD *****\n\n"
    # Set custom SSH port to use
    sudo sed -i "s/^\<Port .*\>/Port $ssh_port/g" "/etc/ssh/sshd_config"
    sudo sed -i "s/^#\<Port .*\>/Port $ssh_port/g" "/etc/ssh/sshd_config"
    # Enable public key authentication
    sudo sed -i "s/^\<PubkeyAuthentication .*\>/PubkeyAuthentication yes/g" "/etc/ssh/sshd_config"
    sudo sed -i "s/^#\<PubkeyAuthentication .*\>/PubkeyAuthentication yes/g" "/etc/ssh/sshd_config"
    # Disable password authentication
    sudo sed -i "s/^\<PasswordAuthentication .*\>/PasswordAuthentication no/g" "/etc/ssh/sshd_config"
    sudo sed -i "s/^#\<PasswordAuthentication .*\>/PasswordAuthentication no/g" "/etc/ssh/sshd_config"
    # Allow custom SSH port through firewall
    sudo firewall-cmd --permanent --new-service="ssh-$ssh_port"
    sudo firewall-cmd --permanent --service="ssh-$ssh_port" --set-description="SSH service on port $ssh_port"
    sudo firewall-cmd --permanent --service="ssh-$ssh_port" --add-port="$ssh_port/tcp"
    sudo firewall-cmd --permanent --zone="$home_zone_name" --add-service="ssh-$ssh_port"
    sudo firewall-cmd --reload
    # Start SSHD service
    sudo systemctl enable sshd
    sudo systemctl start sshd
fi

# Configure .bashrc
step=14
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Configure .bashrc *****\n\n"
    printf "if [ -d \"$HOME/.local/bin\" ]; then\n    export PATH=\$PATH:$HOME/.local/bin\nfi\n" >> "$HOME/.bashrc"
fi

if [ "$reboot_required" = true ]; then
    printf "\n***** Installation finished, you MUST reboot your system for changes to take effect *****\n\n"
else
    printf "\n***** Installation finished *****\n\n"
fi
popd
rm -rf "$tempdir"
