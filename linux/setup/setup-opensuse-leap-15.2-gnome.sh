#!/bin/bash

#             Matt's Setup and Configuration Files
#               Copyright (C) 2021  Matt Hudson
#                   Email: matt@mjrhudson.uk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

###########################################################################
#           TITLE: setup-opensuse-leap-15.2-gnome.sh                      #
#         AUTHORS: Matt Hudson (matt@mjrhudson.uk)                        #
#     DESCRIPTION: Bash script for configuring a fresh install of         #
#                  OpenSUSE Leap 15.2 (Gnome) to my liking                #
# INITIAL RELEASE: 11-April-2021                                          #
#   LAST MODIFIED: 18-April-2021                                          #
###########################################################################

# Note: I don't use the -y flag for zypper installs as I don't leave this
#       unattended, and just like to double check what is being installed as it
#       happens. Feel free to change this for an automated install.
#       The --auto-agree-with-licenses and --gpg-auto-import-keys options will
#       also be useful in this case for new repos and Nvidia drivers.

# ******************************* START HERE ******************************
# Set configured=true when you have looked over the run options and
# configuration settings for this script and are happy with them. The script
# will not run until this is done. I've included this as a reminder to myself
# to configure my scripts.
configured=false
# *************************************************************************

# Configure run steps
# Set these to either true or false depending on which steps you want to run
run_step[0]=$configured # See above
run_step[1]=true        # Create/remove directories
run_step[2]=true        # Remove bloat
run_step[3]=true        # Install ClamAV
run_step[4]=true        # Configure firewalld
run_step[5]=true        # Install software and media codecs from Packman repositories
run_step[6]=true        # Install Network Manager plugins
run_step[7]=true        # Install git
run_step[8]=true        # Install Thunar as default file manager
run_step[9]=true        # Install VNC software
run_step[10]=true        # Install general software and utilities
run_step[11]=true       # Install TeX Live
run_step[12]=true       # Install development patterns
run_step[13]=true       # (Requires reboot) Install flatpak
run_step[14]=true       # Install Visual Studio Code
run_step[15]=true       # Install Syncthing
run_step[16]=true       # Install MBAT
run_step[17]=true       # Create SSH keys
run_step[18]=true       # Configure git
run_step[19]=true       # Configure SSH
run_step[20]=true       # Configure SSHD
run_step[21]=true       # Configure VNC
run_step[22]=true       # Configure dconf
run_step[23]=true       # Configure Visual Studio Code
run_step[24]=true       # Configure .bashrc
run_step[25]=true       # (Requires reboot) Install Nvidia drivers if using an Nvidia graphics card (automatically ignored if not using one)

if [ "$configured" = false ]; then
    printf "\nConfigure this script first before running it!\n\n"
    exit
fi

# Configure settings
home_zone_name="example" # Name to give to home firewall zone
git_user_name="First Last" # Real name for git commits
git_user_email="example@email.com" # Contact email for git commits
ssh_port="22" # SSH port to use (22 is default, change it to something else)
vnc_ports="5900-5905" # VNC ports to use (590X is default, change it to something else)

# Set $reboot_required to false
reboot_required=false

# Make temporary directory for downloading/extracting
tempdir="/tmp/setup-opensuse-leap-15.2-gnome"
if [ -d "$tempdir" ]; then
    rm -rf "$tempdir"
fi
mkdir "$tempdir"
pushd "$tempdir"

# Create/remove directories
step=1
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Create/remove directories *****\n\n"
    if [ -d "$HOME/bin" ]; then
        rm -rf "$HOME/bin"
    fi
    if [ ! -d "$HOME/.local/bin" ]; then
        mkdir -p "$HOME/.local/bin"
    fi
    if [ ! -d "$HOME/.local/scripts" ]; then
        mkdir -p "$HOME/.local/scripts"
    fi
    if [ ! -d "$HOME/.config/systemd/user" ]; then
        mkdir -p "$HOME/.config/systemd/user"
    fi
fi

# Remove bloat
step=2
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Remove bloat *****\n\n"
    sudo zypper remove -y cheese cheese-lang
    sudo zypper remove -y brasero brasero-lang brasero-nautilus libbrasero-burn3-1 libbrasero-media3-1 libbrasero-utils3-1
    sudo zypper remove -y pidgin
    sudo zypper remove -y gnome-documents gnome-documents-lang gnome-shell-search-provider-documents
    sudo zypper remove -y folks-lang libfolks25 libfolks-eds25 gnome-contacts gnome-contacts-lang gnome-maps gnome-maps-lang gnome-shell-calendar gnome-shell-search-provider-contacts bijiben bijiben-lang gnome-shell-search-provider-bijiben evolution evolution-data-server evolution-data-server-lang evolution-ews evolution-ews-lang evolution-lang libebackend-1_2-10 libebook-1_2-20 libebook-contacts-1_2-3 libecal-2_0-1 libedata-book-1_2-26 libedata-cal-2_0-1 libedataserver-1_2-24 libedataserverui-1_2-2
    sudo zypper remove -y mutt mutt-doc mutt-lang
    sudo zypper remove -y polari polari-lang typelib-1_0-Polari-1_0
    sudo zypper remove -y gnome-shell-search-provider-gnome-weather gnome-weather gnome-weather-lang
fi

# Install ClamAV
step=3
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install ClamAV *****\n\n"
    sudo zypper install clamav
    sudo systemctl enable freshclam
    sudo systemctl start freshclam
    sudo freshclam
    sudo systemctl enable clamd
    sudo systemctl start clamd
fi

# Configure firewalld
step=4
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Configure firewalld *****\n\n"
    sudo firewall-cmd --permanent --new-zone="$home_zone_name"
    sudo firewall-cmd --reload
    sudo firewall-cmd --set-default-zone "$home_zone_name"
fi

# Install software and media codecs from Packman repositories
step=5
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install software and media codecs from Packman repositories *****\n\n"
    sudo zypper addrepo -cfp 90 'https://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Leap_$releasever/' packman
    sudo zypper refresh
    sudo zypper dist-upgrade --from packman --allow-vendor-change
    sudo zypper install --from packman ffmpeg gstreamer-plugins-{good,bad,ugly,libav} libavcodec-full vlc vlc-codecs
fi

# Install Network Manager plugins
step=6
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install Network Manager plugins *****\n\n"
    sudo zypper install NetworkManager-openvpn NetworkManager-vpnc
fi

# Install git
step=7
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install git *****\n\n"
    sudo zypper install git
fi

# Install Thunar as default file manager
step=8
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install Thunar as default file manager *****\n\n"
    sudo zypper install thunar
    xdg-mime default thunar.desktop inode/directory application/x-gnome-saved-search
fi

# Install VNC software
step=9
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install VNC software *****\n\n"
    sudo zypper install tigervnc x11vnc
fi

# Install general software and utilities
step=10
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install general software and utilities *****\n\n"
    sudo zypper install htop zip unzip exfat-utils fuse-exfat neofetch filezilla MozillaThunderbird octave
fi

# Install TeX Live
step=11
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install TeX Live *****\n\n"
    sudo zypper install texlive-scheme-full
fi

# Install development patterns
step=12
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install development patterns *****\n\n"
    sudo zypper install -t pattern devel_basis devel_mono devel_C_C++
fi

# (Requires reboot) Install flatpak
step=13
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. (Requires reboot) Install flatpak *****\n\n"
    sudo zypper install flatpak
    flatpak remote-add --if-not-exists flathub "https://flathub.org/repo/flathub.flatpakrepo"
    reboot_required=true
fi

# Install Visual Studio Code
step=14
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install Visual Studio Code *****\n\n"
    sudo rpm --import "https://packages.microsoft.com/keys/microsoft.asc"
    sudo zypper addrepo "https://packages.microsoft.com/yumrepos/vscode" vscode
    sudo zypper refresh
    sudo zypper install code
fi

# Install Syncthing
step=15
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install Syncthing *****\n\n"
    # Set up for Syncthing v1.15.0 - Syncthing will automatically update itself
    # to the most recent version once installed and running
    syncthing_version="v1.15.0"
    if uname -m | grep -wq "x86_64"; then
        arch_known=true
        syncthing_release="syncthing-linux-amd64-$syncthing_version"
    elif uname -m | grep -wq "i386" || uname -m | grep -wq "i686"; then
        arch_known=true
        syncthing_release="syncthing-linux-386-$syncthing_version"
    else
        arch_known=false
        printf "Error: Could not determine a valid architecture\n\n"
    fi
    if [ "$arch_known" = true ]; then
        wget "https://github.com/syncthing/syncthing/releases/download/$syncthing_version/$syncthing_release.tar.gz"
        if curl -sL "https://github.com/syncthing/syncthing/releases/download/$syncthing_version/sha256sum.txt.asc" | grep "$syncthing_release.tar.gz" | sha256sum -c | grep -wq "$syncthing_release.tar.gz: OK"; then
            tar -xvf "$syncthing_release.tar.gz" -C "."
            sudo mv "$syncthing_release" "/opt/Syncthing"
            sudo ln -s "/opt/Syncthing/syncthing" "/usr/bin/syncthing"
            cp "/opt/Syncthing/etc/linux-systemd/user/syncthing.service" "$HOME/.config/systemd/user/syncthing.service"
            sleep 1
            systemctl --user enable syncthing
            systemctl --user start syncthing
            sudo firewall-cmd --zone="$home_zone_name" --add-service=syncthing --permanent
            sudo firewall-cmd --zone="$home_zone_name" --add-service=syncthing-gui --permanent
            sudo firewall-cmd --reload
        else
            printf "Error: SHA256 checksum verification failed for $syncthing_release.tar.gz\n\n"
        fi
        rm -f "$syncthing_release.tar.gz"
    fi
fi

# Install MBAT
step=16
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Install MBAT *****\n\n"
    git clone "https://gitlab.com/mjrhudson/mbat.git" "./mbat"
    pushd "./mbat"
    chmod +x "install-unix.sh"
    bash "./install-unix.sh"
    popd
    rm -rf "./mbat"
fi

# Create SSH keys
step=17
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Create SSH keys *****\n\n"
    ssh-keygen -t ed25519 -a 100 -f "$HOME/.ssh/id_ed25519" -C "$USER@$HOSTNAME"
    ssh-keygen -t ed25519 -a 100 -f "$HOME/.ssh/git_ed25519" -C "$USER@$HOSTNAME"
fi

# Configure git
step=18
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Configure git *****\n\n"
    git config --global user.name "$git_user_name"
    git config --global user.email "$git_user_email"
    git config --global core.editor "nano"
fi

# Configure SSH
step=19
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Configure SSH *****\n\n"
    if [ -f "$HOME/.ssh/config" ]; then
        rm -f "$HOME/.ssh/config"
    fi
    wget -P "$HOME/.ssh" "https://gitlab.com/mjrhudson/matts-setup-and-config-files/-/raw/master/linux/ssh/config"
    chmod 600 "$HOME/.ssh/config"
fi

# Configure SSHD
step=20
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Configure SSHD *****\n\n"
    # Set custom SSH port to use
    sudo sed -i "s/^\<Port .*\>/Port $ssh_port/g" "/etc/ssh/sshd_config"
    sudo sed -i "s/^#\<Port .*\>/Port $ssh_port/g" "/etc/ssh/sshd_config"
    # Enable public key authentication
    sudo sed -i "s/^\<PubkeyAuthentication .*\>/PubkeyAuthentication yes/g" "/etc/ssh/sshd_config"
    sudo sed -i "s/^#\<PubkeyAuthentication .*\>/PubkeyAuthentication yes/g" "/etc/ssh/sshd_config"
    # Disable password authentication
    sudo sed -i "s/^\<PasswordAuthentication .*\>/PasswordAuthentication no/g" "/etc/ssh/sshd_config"
    sudo sed -i "s/^#\<PasswordAuthentication .*\>/PasswordAuthentication no/g" "/etc/ssh/sshd_config"
    # Allow custom SSH port through firewall
    sudo firewall-cmd --permanent --new-service="ssh-$ssh_port"
    sudo firewall-cmd --permanent --service="ssh-$ssh_port" --set-description="SSH service on port $ssh_port"
    sudo firewall-cmd --permanent --service="ssh-$ssh_port" --add-port="$ssh_port/tcp"
    sudo firewall-cmd --permanent --zone="$home_zone_name" --add-service="ssh-$ssh_port"
    sudo firewall-cmd --reload
    # Start SSHD service
    sudo systemctl enable sshd
    sudo systemctl start sshd
fi

# Configure VNC
step=21
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Configure VNC *****\n\n"
    # Install custom VNC scripts
    mkdir -p "$HOME/.local/scripts/vnc/xvnc"
    mkdir -p "$HOME/.local/scripts/vnc/x11vnc"
    wget -P "$HOME/.local/scripts/vnc/xvnc" "https://gitlab.com/mjrhudson/matts-setup-and-config-files/-/raw/master/linux/vnc/xvnc/xvncsrv.sh"
    wget -P "$HOME/.local/scripts/vnc/xvnc" "https://gitlab.com/mjrhudson/matts-setup-and-config-files/-/raw/master/linux/vnc/xvnc/nohup-xvncsrv.sh"
    wget -P "$HOME/.local/scripts/vnc/x11vnc" "https://gitlab.com/mjrhudson/matts-setup-and-config-files/-/raw/master/linux/vnc/x11vnc/x11vncsrv.sh"
    wget -P "$HOME/.local/scripts/vnc/x11vnc" "https://gitlab.com/mjrhudson/matts-setup-and-config-files/-/raw/master/linux/vnc/x11vnc/nohup-x11vncsrv.sh"
    chmod +x "$HOME/.local/scripts/vnc/xvnc/xvncsrv.sh"
    chmod +x "$HOME/.local/scripts/vnc/xvnc/nohup-xvncsrv.sh"
    chmod +x "$HOME/.local/scripts/vnc/x11vnc/x11vncsrv.sh"
    chmod +x "$HOME/.local/scripts/vnc/x11vnc/nohup-x11vncsrv.sh"
    ln -s "$HOME/.local/scripts/vnc/xvnc/xvncsrv.sh" "$HOME/.local/bin/xvncsrv"
    ln -s "$HOME/.local/scripts/vnc/xvnc/nohup-xvncsrv.sh" "$HOME/.local/bin/nohup-xvncsrv"
    ln -s "$HOME/.local/scripts/vnc/x11vnc/x11vncsrv.sh" "$HOME/.local/bin/x11vncsrv"
    ln -s "$HOME/.local/scripts/vnc/x11vnc/nohup-x11vncsrv.sh" "$HOME/.local/bin/nohup-x11vncsrv"
    # Allow custom VNC ports through firewall
    sudo firewall-cmd --permanent --new-service="vnc-$vnc_ports"
    sudo firewall-cmd --permanent --service="vnc-$vnc_ports" --set-description="VNC service on port $vnc_ports"
    sudo firewall-cmd --permanent --service="vnc-$vnc_ports" --add-port="$vnc_ports/tcp"
    sudo firewall-cmd --permanent --zone="$home_zone_name" --add-service="vnc-$vnc_ports"
    sudo firewall-cmd --reload
fi

# Configure dconf
step=22
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Configure dconf *****\n\n"
    dconf dump / > "$HOME/.config/dconf/install-backup.conf"
    if [ -f "$HOME/.config/dconf/user.conf" ]; then
        rm -f "$HOME/.config/dconf/user.conf"
    fi
    wget -P "$HOME/.config/dconf" "https://gitlab.com/mjrhudson/matts-setup-and-config-files/-/raw/master/linux/dconf/user.conf"
    dconf load / < "$HOME/.config/dconf/user.conf"
fi

# Configure Visual Studio Code
step=23
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Configure Visual Studio Code *****\n\n"
    if [ -f "$HOME/.config/Code/User/settings.json" ]; then
        rm -f "$HOME/.config/Code/User/settings.json"
    fi
    wget -P "$HOME/.config/Code/User" "https://gitlab.com/mjrhudson/matts-setup-and-config-files/-/raw/master/vscode/settings.json"
fi

# Configure .bashrc
step=24
if [ "${run_step[$step]}" = true ]; then
    printf "\n***** Step $step. Configure .bashrc *****\n\n"
    printf "if [ -d \"$HOME/.local/bin\" ]; then\n    export PATH=\$PATH:$HOME/.local/bin\nfi\n" >> "$HOME/.bashrc"
fi

# (Requires reboot) Install Nvidia drivers if using an Nvidia graphics card
#                       - automatically ignored if not using one
step=25
if [ "${run_step[$step]}" = true ]; then
    if sudo hwinfo --gfxcard | grep -iF "model" | grep -iqF "nvidia"; then
        nvidia_detected=true
    else
        nvidia_detected=false
    fi
    if [ "$nvidia_detected" = true ]; then
        printf "\n***** Step $step. (Requires reboot) Install Nvidia drivers *****\n\n"
        sudo zypper addrepo 'https://download.nvidia.com/opensuse/leap/$releasever' NVIDIA
        sudo zypper refresh
        # WARNING: Read the comment after these commands CAREFULLY and uncomment
        #          the correct one; G05 will probably be correct for 90% of
        #          current machines (and any new machine) using an Nvidia
        #          graphics card.
        
        #          If unsure just comment out both and see:
        #          https://en.opensuse.org/SDB:NVIDIA_drivers
        
        sudo zypper install x11-video-nvidiaG05 nvidia-glG05 # Nvidia 600 series or newer
        # sudo zypper install x11-video-nvidiaG04 nvidia-glG04 # Nvidia 400 series or newer
        reboot_required=true
    fi
fi

if [ "$reboot_required" = true ]; then
    printf "\n***** Installation finished, you MUST reboot your system for changes to take effect *****\n\n"
else
    printf "\n***** Installation finished *****\n\n"
fi
popd
rm -rf "$tempdir"
