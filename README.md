# Matt's Setup and Configuration Files

What it says on the tin!

I keep this repository as an easy way to set up new machines and port across configurations I have for various programs. I doubt these will be useful to anyone else, but feel free to take a look!

The Linux setup scripts will probably be the most useful; I have attempted to generalise them where possible.
